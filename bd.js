var bd = "https://bd-api.ncsa.illinois.edu";
var bdFiddle = "https://browndog.ncsa.illinois.edu/bdfiddle";
var token = getCookie('token');

console.log(bd);
console.log(token);

//Load JQuery
//if(!window.jQuery) {
	var jq = document.createElement('script');
	jq.type = 'text/javascript';
	jq.addEventListener('load', checkAuthorization);
	jq.src = "https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
	document.getElementsByTagName('head')[0].appendChild(jq);
//} else {
//	checkAuthorization();
//}

//Check if authorization is needed before attempting to add conversion menus
function checkAuthorization() {

    // Check validity of token only if its value is not null or emtpy string
    if (token != null && token != "") {
        $.ajax({
            url: bd + '/v1/tokens/' + token,
            success: function (data, textStatus, jqXHR) {
                // If token is found
                if (data["found"] == "true") {
                    processLinks();
                }
                // If token is not found
                else {
                    alert("The provided token is either invalid or has expired. Please try again.");
                    promptUserForTokenAndValidate();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('Status: ' + jqXHR.status + ', ' + errorThrown + ', ' + textStatus);

                // In case of error, prompt user for token
                alert("The provided token is either invalid or has expired. Please try again.");
                promptUserForTokenAndValidate();
            }
        });
    }
    else {
        promptUserForTokenAndValidate();
    }
}

// Prompt user for Brown Dog token and validate
function promptUserForTokenAndValidate(){
    token = prompt("Enter your Brown Dog API (" + bd + ") token here: ");
    // Set cookie only if the value of token is not null or empty string
    if (token != null && token != "") {
        document.cookie = "token=" + token;
        checkAuthorization();
    }
    else {
        // Show alert box describing how to get a Brown Dog token
        alert("Getting a Brown Dog Token:\n\n" +
            "If you have a Brown Dog account, you can get a token by either visiting BD Fiddle " +
            "(" + bdFiddle + ") or by using the BD API (" + bd + ") using your Brown Dog credentials. " +
            "Please visit either of these URLs for more details.");
    }
}

//Process webpage once loaded
function processLinks() {
	addGraphic();

	//Convert links on click
	$("a[dap]").each(function() {
		$(this).data('href', this.href);
		$(this).data('output', $(this).attr('dap'));
		$(this).attr('href', '#');
		$(this).on('click', convert)
	});

	//Convert images right away	
	$("img[dap]").each(function() {
		console.log(this.src + ' -> ' + $(this).attr('dap'));
		var img = this;

		$.ajax({
    	    headers: {Accept: "text/plain", Authorization: token},
			url: bd + '/v1/conversions/' + $(this).attr('dap') + '/' + encodeURIComponent(this.src)
		}).then(function(result) {
  		$(img).css('cursor', 'wait');		//Set cursor to wait
			reload(img, result);
		});
	});

	//Extract links right away
	$("a[dts]").each(function() {
		var a = this;
		console.log(a.href + ' -> dts');

		$.ajax({
			type: "POST",
    	    headers: {Authorization: token},
			contentType: "application/json; charset=utf-8",
			url: bd + '/v1/extractions/url',
			data: JSON.stringify({"fileurl": a.href})
		}).then(function(result) {
			console.log('id: ' + result.id);
  		    $(a).css('cursor', 'wait');		//Set cursor to wait
			waitForMetadata(a, result.id);
		});

	});
}

//Wait for then set links title to extraction results
function waitForMetadata(a, id) {
  $.ajax({
    headers: {Authorization: token},
		url: bd + '/v1/extractions/' + id + '/status'
  }).then(function(result) {
		if(result.Status == "Done") {
			console.log('Extraction complete: ' + id);
  		
			$.ajax({
    		headers: {Authorization: token},
				url: bd + '/v1/extractions/files/' + id + '/metadata.jsonld'
			}).then(function(result) {
				$(a).attr('title', JSON.stringify(result));
      	$(a).css('cursor', 'default');    //Set cursor to default
			});
		}else{
			console.log('Waiting for extraction: ' + id);
    	setTimeout(function() {waitForMetadata(a, id);}, 1000);
		}
	});
}

//Issue a conversion request when a link is clicked
function convert() {
	console.log($(this).data('href') + ' -> ' + $(this).data('output'));

	//Set cursor to wait
	$('html,body').css('cursor', 'wait');
	$('a').each(function() {
		$(this).css('cursor', 'wait');
	});

	$.ajax({
    headers: {Accept: "text/plain", Authorization: token},
		url: bd + '/v1/conversions/' + $(this).data('output') + '/' + encodeURIComponent($(this).data('href'))
	}).then(function(result) {
		redirect(result);
	});
}

//Redirect to the given url once it exists
function redirect(url) {
	console.log('Waiting for output: ' + url);

	$.ajax({
		//type: 'HEAD',
   	headers: {Authorization: token},
   	url: url,
		success: function() {
			console.log('Redirected: ' + url);
			window.location.href = addAuthentication(url);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Status: ' + jqXHR.status + ', ' + errorThrown);

			if(jqXHR.status == 0) {
				console.log('Redirected: ' + url);
				window.location.href = addAuthentication(url);
			}else{
				setTimeout(function() {redirect(url);}, 1000);
      }
		}
	});
}

//Redirect the image once it source exists
function reload(img, url) {
	//console.log(img.src + ', ' + url);

	$.ajax({
		//type: 'HEAD',
   	headers: {Authorization: token},
   	url: url,
		success: function() {
			console.log('Reloading: ' + url);
  		$(img).css('cursor', 'default');		//Set cursor to default
			img.src = addAuthentication(url);
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Status: ' + jqXHR.status + ', ' + errorThrown);

			if(jqXHR.status == 0) {
				console.log('Reloading: ' + url);
  			$(img).css('cursor', 'default');		//Set cursor to default
				img.src = addAuthentication(url);
			}else{
				setTimeout(function() {reload(img, url);}, 1000);
			}
		}
	});
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');

	for(var i=0; i<ca.length; i++) {
		var c = ca[i];
		while(c.charAt(0)==' ') c = c.substring(1);
		if(c.indexOf(name) == 0) return c.substring(name.length, c.length);
	}

	return null;
}

function addAuthentication(url) {
	if(token) {
		url = url + '?token=' + token;
	}

	return url;
}

//Brown Dog graphic
function addGraphic() {
	//Preload images
	$.get('http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif');
	$.get('http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif');

	var graphic = $('<img>')
		.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/browndog-small-transparent.gif')
		.attr('width', '25')
		.attr('id', 'graphic')
		.css('position', 'absolute')
		.css('left', '0px')
		.css('bottom', '25px');
	$("body").append(graphic);

	setTimeout(moveGraphicRight, 10);
}

function moveGraphicRight() {
	var graphic = document.getElementById('graphic');
	graphic.style.left = parseInt(graphic.style.left) + 25 + 'px';

	if(parseInt(graphic.style.left) < $(window).width() - 50) {
		setTimeout(moveGraphicRight, 10);
	} else {
		graphic.remove();

		//Add powered by graphic
		graphic = $('<img>')
			.attr('src', 'http://browndog.ncsa.illinois.edu/graphics/PoweredBy-transparent.gif')
			.attr('width', '100');

		var link = $('<a/>')
			.attr('href', 'http://browndog.ncsa.illinois.edu')
			.attr('id', 'poweredby')
			.css('position', 'fixed')
			.css('right', '10px')
			.css('bottom', '10px')
			.append(graphic);

		$("body").append(link);
	}
}
